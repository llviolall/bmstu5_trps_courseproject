<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\SignupController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('main');

Route::post('/signup', 'App\Http\Controllers\SignupController@signup')->name('signup');
Route::get('/journal', 'App\Http\Controllers\SignupController@list')->middleware(['auth'])->name('journal');

require __DIR__ . '/auth.php';
