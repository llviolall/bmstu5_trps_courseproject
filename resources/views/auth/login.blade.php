<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Viola Beauty Club</title>
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Forum&display=swap" rel="stylesheet">
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav">
        <div class="container px-5">
            <a class="navbar-brand fw-bold" href="#page-top">Viola Beauty Club</a>
            </div>
        </div>
    </nav>
    <section class="cta">

        <div class="cta-content">
            <div class="container px-5">
                <form id="authForm" method="POST" action="{{ route('login') }}">
                    @csrf
                    <!-- Email address input-->
                    <div class="form-floating mb-3">
                        <input class="form-control" name="email" id="email" type="email"
                            placeholder="name@example.com" required="required"/>
                        <label for="email">Электронная почта"</label>
                    </div>
                    <!-- Phone number input-->
                    <div class="form-floating mb-3">
                        <input class="form-control" name="password" id="password" type="password" required="required"/>
                        <label for="password">Пароль</label>
                    </div>

                    <!-- Submit Button-->
                    <div class="d-grid"><button class="btn btn-primary rounded-pill btn-lg"
                            id="submitButton" type="submit">Войти</button></div>
                </form>
            </div>
        </div>
    </section>
   
    <!-- Footer-->
    <footer class="bg-black text-center py-5">
        <div class="container px-5">
            <div class="text-white-50 small">
                <div class="mb-2">&copy; Viola Beauty Club 2022</div>
                <div class="mb-2">
                    @auth
                        <a href="{{ url('/journal') }}" class="btn btn-link">Журнал записей</a>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <button class="btn btn-link" onclick="event.preventDefault();
                                                                            this.closest('form').submit();">Выход
                            </button>
                        </form>
                    @else
                        <button class="btn btn-link" data-bs-toggle="modal" data-bs-target="#authModal">
                            Вход для администратора
                        </button>
                    @endauth



                </div>
            </div>
        </div>
    </footer>
    <!-- Feedback Modal-->
    <div class="modal fade" id="signupModal" tabindex="-1" aria-labelledby="feedbackModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-gradient-primary-to-secondary p-4">
                    <h5 class="modal-title font-alt" id="feedbackModalLabel">Записаться</h5>
                    <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <div class="modal-body border-0 p-4">
                    <form method="POST" action="{{ route('signup') }}">
                        @csrf
                        <!-- Name input-->
                        <div class="form-floating mb-3">
                            <input class="form-control" name="name" id="name" type="text"
                                placeholder="Как вас зовут..." required="required"/>
                            <label for="name">Как вас зовут</label>
                        </div>
                        <!-- Phone number input-->
                        <div class="form-floating mb-3">
                            <input class="form-control" name="phone" id="phone" type="tel"
                                placeholder="+7 (123) 456-78-90" required="required"/>
                            <label for="phone">Номер телефона</label>
                        </div>
                        <!-- Услуга -->
                        <div class="form-floating mb-3">
                            <select class="form-control" name="service" id="service" type="text" />
                            <option value="Косметология">Косметология</option>
                            <option value="Маникюр  педикюр">Маникюр педикюр</option>
                            <option value="Макияж и оформление бровей">Макияж и оформление бровей</option>
                            <option value="Парикмахерские услуги">Парикмахерские услуги</option>
                            </select>
                            <label for="service">Услуга</label>
                        </div>
                        <div class="d-grid"><button class="btn btn-primary rounded-pill btn-lg"
                                id="submitButton" type="submit">Записаться</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="authModal" tabindex="-1" aria-labelledby="authModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-gradient-primary-to-secondary p-4">
                    <h5 class="modal-title font-alt" id="authModalLabel">Вход для администратора</h5>
                    <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <div class="modal-body border-0 p-4">
                    <form id="authForm" method="POST" action="{{ route('login') }}">
                        @csrf
                        <!-- Email address input-->
                        <div class="form-floating mb-3">
                            <input class="form-control" name="email" id="email" type="email"
                                placeholder="name@example.com" required="required"/>
                            <label for="email">Электронная почта"</label>
                        </div>
                        <!-- Phone number input-->
                        <div class="form-floating mb-3">
                            <input class="form-control" name="password" id="password" type="password" required="required"/>
                            <label for="password">Пароль</label>
                        </div>

                        <!-- Submit Button-->
                        <div class="d-grid"><button class="btn btn-primary rounded-pill btn-lg"
                                id="submitButton" type="submit">Войти</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    @if (session('status'))
        <script>
            var myModal = new bootstrap.Modal(document.getElementById('successModal'))
            myModal.show()
        </script>
    @endif
</body>

</html>
