<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Journal;

class SignupController extends Controller
{
    public function signup(Request $request)
    {
        // dd($request);
        $signup = new Journal;
        $signup->name = $request->name;
        $signup->phone = $request->phone;
        $signup->service = $request->service;
        $signup->save();
        return redirect('/')->with('status', 'Вы успешно записались в наш салон красоты. В ближайшее время с вами свяжется администратор!');
    }
    public function list()
    {
        $signups = Journal::get();
        return view('dashboard', compact('signups'));
    }
}
